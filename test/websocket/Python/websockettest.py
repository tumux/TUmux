import asyncio
import websockets

async def test():
    async with websockets.connect('://localhost:2629') as websocket:
        websocket.send('test msg')
        msg = await websocket.recv()
        print(f"From Server: {msg}")
asyncio.get_event_loop().run_until_complete(test())
