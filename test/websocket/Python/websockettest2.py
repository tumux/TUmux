import asyncio
import websockets
import json

msg = {"Command":"Read.CVT"}
# msg = {"Command":"Read.Errors"}
# msg = {"Command":"Clear.Errors"}
# msg = {"Command":"Write.ActChannel","Act. channel":5}
# msg = {"Command":"Write.Recipe","Recipe":[[1,0,11],[0,0,12],[2,0,13],[1,0,14]]}
# msg = {"Command":"Debug"}

async def call_api(msg):
    async with websockets.connect('ws://10.50.44.238:2629') as websocket:
        await websocket.send(msg)
        response = await websocket.recv()
        print(response)
asyncio.get_event_loop().run_until_complete(call_api(json.dumps(msg)))