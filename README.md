# TUmux off-gas multiplexer

Embedded software for control of a off-gas multiplexer based on National Instruments embedded controllers (CompactRIO).

## Dependencies

- LabVIEW 2021
  - This version doesn't support compactFieldPoint anymore. Last TUmux version with cFP support is 2.6, based on LabVIEW 2015.
- NI Modbus Library
- OpenG Toolkit

## License

This project is licenced under the [EUPL Licence](LICENCE.md).

<a href="https://www.flaticon.com/free-icons/sensors" title="sensors icons">Sensors icons created by Iconjam - Flaticon</a>

## Contact

For any questions or inquiries, please contact Dirk Geerts (d.j.m.geerts@tudelft.nl).

## Installatie cRIO

### Voorbereiden cRIO
In MAX
- install cRIO 18.5 (laatste versie met support voor cRIO-9101) (cRIO-9113 - 19.5)
  + NI VISA
  + Remote panel
  + NI system configuration

In LabVIEW IDE
- In project - Properties cRIO module - Enable Remote Panel server (op port 8000, default)
- Deploy

In project under www 
- change tumux-[x].html to the right number (save file)
- in the file replace [x] to the right number (use notepad)

Voeg de volgende file toe aan project\my computer\www\files
- Runtime engine (staat op K:\bt\ftd\labview\Projects\Tumux\www\files)